<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('authapi')->group(function () {

    Route::prefix('posts')->group(function () {

        Route::get('/','PostsController@list');
        Route::get('/{id}','PostsController@item');
        Route::post('/', 'PostsController@create');
        Route::post('/{id}', 'PostsController@update');
        Route::delete('/{id}', 'PostsController@remove');
    });
});
