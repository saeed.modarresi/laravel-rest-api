<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostsRequestValidation;
use App\Models\Post;

class PostsController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Get posts
    |--------------------------------------------------------------------------
    */
    public function list()
    {
        return Post::query()->get()->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Get one post details
    |--------------------------------------------------------------------------
    */
    public function item($id)
    {
        return Post::query()->where('id', $id)->get()->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Create post
    |--------------------------------------------------------------------------
    */
    public function create(PostsRequestValidation $request)
    {

        $data = $request->validated();

        $response = Post::query()->create([
            'post_author' => data_get($data, 'post_author'),
            'post_title'  => data_get($data, 'post_title'),
            'post_description' => data_get($data, 'post_description'),
        ]);

        if (!$response) return ['An error has occurred.'];
        return ['The post was created successfully.'];
    }

    /*
    |--------------------------------------------------------------------------
    | Update post
    |--------------------------------------------------------------------------
    */
    public function update(PostsRequestValidation $request, $id)
    {

        $data = $request->validated();

        $response = Post::query()
            ->where('id', $id)
            ->update([
                'post_author' => data_get($data, 'post_author'),
                'post_title'  => data_get($data, 'post_title'),
                'post_description' => data_get($data, 'post_description'),
            ]);

        if (!$response) return ['An error has occurred.'];
        return ['The post was updated successfully.'];
    }

    /*
    |--------------------------------------------------------------------------
    | Remove post
    |--------------------------------------------------------------------------
    */
    public function remove($id)
    {
        $response = Post::query()->where('id', $id)->delete();
        if (!$response) return ['An error has occurred.'];
        return ['The post was removed successfully.'];
    }
}
