<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        /*
        |--------------------------------------------------------------------------
        | Check token
        |--------------------------------------------------------------------------
        |
        | Check the token, if it's validated, run the next step.
        |
        */
        $bearerToken = $request->bearerToken();

        if (empty($bearerToken)) return response()->json(['message' => 'Authentication token is needed']);
        if ($bearerToken != env('API_KEY')) return response()->json(['message' => 'Authentication token is wrong']);

        return $next($request);
    }
}
