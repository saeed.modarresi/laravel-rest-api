<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostsControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_show_posts_list()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. env('API_KEY'),
        ])->json('GET','api/posts');
        $response->assertStatus(200);
    }

    public function test_show_one_post_data()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. env('API_KEY'),
        ])->json('GET','api/posts/1');
        $response->assertStatus(200);
    }

    public function test_create_post()
    {

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. env('API_KEY'),
        ])->json('POST','api/posts',[
            'post_author' => 'saeed modarresi',
            'post_title' => 'post title test',
            'post_description' => 'post description test'
        ]);
        $response->assertStatus(200);
    }

    public function test_update_post()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. env('API_KEY'),
        ])->json('POST','api/posts/1',[
            'post_author' => 'saeed modarresi',
            'post_title' => 'post title test',
            'post_description' => 'post description test'
        ]);
        $response->assertStatus(200);
    }

    public function test_remove_post()
    {

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. env('API_KEY'),
        ])->json('DELETE','api/posts/1');

        $response->assertStatus(200);
    }
}
